package com.hfax.hfax.appium.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.pagefactory.AndroidFindBy;

public class StartPage {
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.Button[2]")
	public  WebElement mLoginButton;
	
	public  WebElement RegisterButton;
	
	public StartPage(WebDriver driver) {
        super();
    }
	public void goToLoginView(){
//		Point location = menuButton.getLocation();
//		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
		mLoginButton.click();
	}
}
