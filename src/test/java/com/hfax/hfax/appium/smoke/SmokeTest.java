package com.hfax.hfax.appium.smoke;

import com.hfax.base.BaseTest;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import static junit.framework.TestCase.*;
import java.util.List;

/**
 * Created by philip on 2/24/16.
 */
public class SmokeTest extends BaseTest {
   // @Test
    public void testSplashScreen() throws Exception {
        List<WebElement> elements = driver.findElementsByAccessibilityId("aaa");
        assertEquals(elements.size() == 0, true);
    }

   // @Test
    public void testTabBar() throws Exception {
        List<WebElement> elements = driver.findElementsByAccessibilityId("aaa");
        assertEquals(elements.size() == 0, true);
    }
}
