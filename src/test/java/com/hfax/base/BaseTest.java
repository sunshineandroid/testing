package com.hfax.base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.TimeOutDuration;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import com.hfax.hfax.appium.page.StartPage;

import com.hfax.hfax.appium.regression.Configuration;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by philip on 2/24/16.
 */
public class BaseTest {
    protected AppiumDriver<WebElement> driver;
    protected int screenWidth, screenHeight;
    private StartPage mStartPage;
    @Before
    public void setUp() throws Exception {
        Map<String, String> env = System.getenv();

        String apkPath = env.get("APK_PATH");
        assertEquals("Apk path is empty", apkPath != null, true);

        File checkApkPath = new File(apkPath);
        assertEquals("Apk file does not exist", checkApkPath.exists(), true);

        String serverPath = env.get("SERVER_PATH");

        if (serverPath == null || serverPath.isEmpty()) {
            serverPath = "http://127.0.0.1:4723/wd/hub";
        }

        File app = new File(apkPath);
        assertEquals("Apk path is empty", apkPath != null, true);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName","");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("noSign", true);
        capabilities.setCapability("app", apkPath);
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        screenWidth = driver.manage().window().getSize().width;
        screenHeight = driver.manage().window().getSize().height;
        mStartPage = new StartPage(driver);
    }
    
    @After
    public void tearDown() throws Exception {
        if (driver != null) {
           driver.quit();
        }
    }
    
    
    public void swipeSplash() throws InterruptedException{
    	Thread.sleep(3000);
    	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement slider = driver.findElementByClassName("android.widget.LinearLayout");
        Point upperLeft = slider.getLocation();
    	Dimension dimensions = slider.getSize();
    	Point sliderLocation = new Point(upperLeft.getX() + dimensions.getWidth() / 2, upperLeft.getY() + dimensions.getHeight() / 2);
    	for (int i = 0; i < 4; i ++) {
    		if (i != 3) {
    			driver.swipe(sliderLocation.getX() * 2 - 50, sliderLocation.getY(), sliderLocation.getX() / 10, sliderLocation.getY(), 500);
    		} else {
    			slider.click();
    		}
    	}
    }
    
    public void loginHfax(String phoneNumber,String password) throws InterruptedException{
    	swipeSplash();
    	driver.findElementsByClassName("android.widget.Button").get(1).click();
    	driver.findElementByXPath("//android.view.View/android.widget.Image[1]").click();
        WebElement name = driver.findElementByXPath("//android.view.View/android.widget.EditText[1]");
        name.sendKeys(phoneNumber);
        WebElement passwd = driver.findElementByXPath("//android.view.View/android.widget.EditText[2]");
        passwd.sendKeys(password);
        driver.findElementByXPath("//android.view.View/android.view.View[2]").click();
        
    }
    
    public void leftTopButton(){
//    	WebElement leftButton  = driver.findElementByXPath(" //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.TabHost[1]/android.widget.RelativeLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.webkit.WebView[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]");
//    	leftButton.click();
    	TouchAction gesture = new TouchAction(driver).press(100, 100).release();
    	driver.performTouchAction(gesture);
    }
    
    public void rightTopButton(){
    	TouchAction gesture = new TouchAction(driver).press(screenWidth - 30, 100).release();
    	driver.performTouchAction(gesture);
    }
    
    public void menuSelect(String menu) throws InterruptedException{
    	loginHfax(Configuration.phoneNumber, Configuration.loginpassword);
    	driver.findElementByXPath("//android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]").click();;
    	driver.findElementByName(menu).click();
    }  
    
    
}
